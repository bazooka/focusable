class FocusableSelector {

  constructor (wrapper) {
    this.currentWrapper = wrapper
    this.setCurrentWrapper()
  }

  /**
   * @param FocusableWrapper focusableWrapper
   */
  setCurrentWrapper (focusableWrapper) {
    this.previousWrapper = this.currentWrapper
    this.previousWrapper.disable()
    this.currentWrapper = focusableWrapper
    this.currentWrapper.enable()
  }

  restorePreviousWrapper () {
    this.currentWrapper = this.previousWrapper
    this.currentWrapper.restore()
  }
}

export default FocusableSelector
