class FocusableStack {

    /**
     * @param FocusableElement focusableElement
     */
    constructor(focusableElement) {
        focusableElement.init()
        this.currentFocusableElement = focusableElement
        this.previousFocusableElements = []
        this.allFocusableElements = [focusableElement]
    }

    /**
     * @param FocusableElement focusableElement
     */
    setPrevious(focusableElement) {
        focusableElement.disableFocus()
        this.previousFocusableElements.push({element: focusableElement, trigger: document.activeElement});
    }

    getPrevious() {
        if (this.previousFocusableElements.length > 0) {
            return this.previousFocusableElements.pop()
        }

        return this.currentFocusableElement
    }

    /**
     * @param FocusableElement focusableWrapper
     */
    setCurrent(focusableElement) {
        focusableElement.init()
        this.setPrevious(this.currentFocusableElement)
        this.currentFocusableElement = focusableElement
        this.currentFocusableElement.enableFocus()

        if (this.currentFocusableElement.element.getAttribute('tabindex') !== null) {
            this.currentFocusableElement.element.setAttribute('tabindex', '0');
        }

        this.currentFocusableElement.element.focus()
    }

    restorePrevious() {
        let previous = this.getPrevious();
        this.currentFocusableElement = previous.element
        this.currentFocusableElement.restoreFocus()

        this.allFocusableElements.forEach(focusableElement => {
            if (focusableElement.element !== previous.element) {
                focusableElement.restoreFocus();
            }
        })

        // Restore the focus to the element that was focused before the current element
        previous.trigger.focus()
    }

    add(focusableElement) {
        this.allFocusableElements.push(focusableElement)
    }
}

export default FocusableStack
