const focusables = '*[data-shdw], a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, *[tabindex], *[contenteditable]'

class FocusableElement {

    /**
     * @param HTMLElement el
     */
    constructor(el) {

        if (!el) {
            console.warn('No element found', el)
            return
        }

        this.element = el
        this.focusables = [];

        this.init = this.init.bind(this);
        this.disableFocus = this.disableFocus.bind(this);
        this.enableFocus = this.enableFocus.bind(this);
        this.restoreFocus = this.restoreFocus.bind(this);
        this.updateDefaultTabIndex = this.updateDefaultTabIndex.bind(this);
    }

    init() {
        this.updateDefaultTabIndex();
    }

    updateDefaultTabIndex() {
        let elsFocusable = Array.from(this.element.querySelectorAll(focusables))
        this.focusables = elsFocusable.map((focusable) => {
            return {
                element: focusable,
                defaultTabIndex: focusable.getAttribute('tabindex')
            }
        })
    }

    forceDefaultTabIndex(tabIndex) {
        this.focusables = this.focusables.map((focusable) => {
            focusable.defaultTabIndex = tabIndex;
            return focusable;
        })
    }

    /**
     *
     * @param Event e
     */
    trapFocus(e) {
        if (e.defaultPrevented) {
            return // Do nothing if the event was already processed
        }

        // Capture focus when Tab or Maj+Tab in Close
        if (e.keyCode === 9 && this.isOpen && this.focusables.includes(e.target)) {
            // Focus last element when Maj+Tab on first focusable
            if (e.shiftKey) {
                if (e.target === this.focusables[0]) {
                    this.focusables[this.focusables.length - 1].element.focus()
                }
            } else {
                // Focus first element when Tab on last focusable
                if (e.target === this.focusables[this.focusables.length - 1]) {
                    this.focusables[0].element.focus()
                }
            }
        }
    }

    disableFocus() {
        this.focusables.map((focusable) => {
            focusable.element.setAttribute('tabindex', '-1')
        })
    }

    restoreFocus() {
        this.focusables.map((focusable) => {
            if (focusable.defaultTabIndex !== null) {
                focusable.element.setAttribute('tabindex', focusable.defaultTabIndex)
            } else {
                focusable.element.removeAttribute('tabindex')
            }
        })
    }

    enableFocus() {
        this.focusables.map((focusable) => {
            focusable.element.setAttribute('tabindex', '0')
        })
    }
}

export default FocusableElement
