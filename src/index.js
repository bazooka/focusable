import FocusableElement from './FocusableElement'
import FocusableStack from './FocusableStack'

let bodyWrapper = new FocusableElement(document.body)
let focusableStack = new FocusableStack(bodyWrapper)

export {
  focusableStack,
  FocusableElement,
  FocusableStack
}
