If you want to manage focusable element in your DOM with Vanilla JS.

## 📦 How to install this project ?
```bash
npm install @fantassin/focusable
```

## ⚡ How it works ?
You can declare focusable element by its container. All focusables are detected automatically.
The focusableStack keep in memory focuses history. 
```js
import { focusableStack, FocusableElement } from '@fantassin/focusable';

let element = new FocusableElement( document.querySelector('.container') );

// element.trapFocus();
// element.disableFocus();
// element.restoreFocus();
// element.enableFocus();
// element.trapFocus();

// focusableStack.setCurrent(element);
// focusableStack.restorePrevious();

```
